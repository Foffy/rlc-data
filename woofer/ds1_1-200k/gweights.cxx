#include <fstream>
//vin = 2V
constexpr double residualgain = 70.;
constexpr double residualoffset = 20.;
constexpr double Tint = 1.;
constexpr double Text = 10.;
constexpr double noise = 57.;
constexpr double gaintempco = 13.;
constexpr double offsettempco = 24.;
constexpr double range = 2.;
///////////////////////////////////////////////////////////////////////////
//AI ACCURACY FORMULA:                                                   //
//gainerror = residualgain + gaintempco * Tint + Text (ppm)              //
//offseterror = residualoffset + offsettempco * Tint + 60 (ppm)          //
//noiseuncertainty = (noise * 3) / 10 (uV)                               //
//accuracy = x * gainerror + range * offseterror + noiseuncertainty (uV) //
/////////////////////////////////////////////////////////////////////////// 
double getsigma(double x, double vin)
{
	double gainerror = residualgain + gaintempco * Tint + Text;
	double offseterror = residualoffset + offsettempco * Tint + 60.;
	double noiseuncertainty = ((noise * 3) / 10)/1e6;
	return (x * gainerror/1e6 + range * offseterror/1e6 + noiseuncertainty);
}

int main()
{
	std::fstream data("wsource.txt");
	std::ofstream sigma("sigmas.txt");
	while(data.good())
	{
		double f;
		double x;
		double p;

		//double vin;
		data >> f >> x >> p; //>> vin;
		sigma << f << '\t' << x*2. << '\t' << p << '\t' << getsigma(x*2,2. /*vin*/) << '\n';
	}
}
